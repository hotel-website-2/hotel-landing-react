import React, { useState, useEffect, useContext } from 'react';
import { Table, Button, Modal, Form } from 'react-bootstrap';
import UserContext from '../UserContext';
import { useParams, Link } from 'react-router-dom';
import Swal from "sweetalert2"

const ProductTable = ({ dashProp }) => {
  const { _id, name, capacity, description, price, numberOfStock, image } = dashProp;
  const [status, setStatus] = useState('');
  const [showModal1, setShowModal1] = useState(false);
  const [showModal2, setShowModal2] = useState(false);
  const [updatedName, setUpdatedName] = useState(name);
  const [updatedDescription, setUpdatedDescription] = useState(description);
  const [updatedPrice, setUpdatedPrice] = useState(price);
  const [updatedStock, setUpdatedStock] = useState(capacity);
  const [addedName, setAddedName] = useState('');
  const [addedDescription, setAddedDescription] = useState('');
  const [addedPrice, setAddedPrice] = useState('');
  const [addedStock, setAddedStock] = useState('');
  const [addedImage, setAddedImage] = useState('');
  const { user } = useContext(UserContext);

  useEffect(() => {
    if (status === '') {
      setStatus(dashProp.status);
    }
  }, [dashProp.status]);

  const updateProduct = () => {
    const newStatus = status === 'Active' ? 'notActive' : 'Active';

    fetch(`${process.env.REACT_APP_API_URL}/product/${_id}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${user.token}`,
      },
      body: JSON.stringify({
        name: updatedName,
        description: updatedDescription,
        price: updatedPrice,
        capacity: updatedStock,
        status: newStatus,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setStatus(newStatus);
        handleCloseModal1();
      })
      .catch((error) => {
        console.error('Error:', error);
      });
  };

  const handleCloseModal1 = () => {
    setShowModal1(false);
  };

  const handleShowModal1 = () => {
    setShowModal1(true);
  };

  const handleCloseModal2 = () => {
    setShowModal2(false);
  };

  const handleShowModal2 = () => {
    setShowModal2(true);
  };

  const addProduct = () => {
    fetch(`${process.env.REACT_APP_API_URL}/product/registerProducts`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${user.token}`,
      },
      body: JSON.stringify({
        name: addedName,
        description: addedDescription,
        image: addedImage,
        price: addedPrice,
        capacity: addedStock,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        handleCloseModal2();
        Swal.fire({
          icon: 'success',
          title: 'Added Successfully!',
          text: 'Thank you!',
        })
      })
      .catch((error) => {
        console.error('Error:', error);
      });
  };

  return (
    <>
      <Table striped>
        <thead>
          <tr>
            <th>_id</th>
            <th>name</th>
            <th>image</th>
            <th>description</th>
            <th>price</th>
            <th>Capacity</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>{_id}</td>
            <td>{name}</td>
            <td><img src={image} alt={name} style={{ width: '100px' }} /></td>
            <td>{description}</td>
            <td>{price}</td>
            <td>{capacity}</td>
            <td>
              <Button type="button" onClick={updateProduct} className="m-2">
                {status}
              </Button>
              <Button type="button" onClick={handleShowModal1} className="bg-danger m-2">
                Update
              </Button>
              <Button type="button" onClick={handleShowModal2} className="bg-warning">
                Add
              </Button>
            </td>
          </tr>
        </tbody>
      </Table>

      <Modal show={showModal1} onHide={handleCloseModal1}>
        <Modal.Header closeButton>
          <Modal.Title>Update Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form.Group>
            <Form.Label>Name</Form.Label>
            <Form.Control
              type="text"
              value={updatedName}
              onChange={(e) => setUpdatedName(e.target.value)}
            />
          </Form.Group>

          <Form.Group>
            <Form.Label>Description</Form.Label>
            <Form.Control
              type="text"
              value={updatedDescription}
              onChange={(e) => setUpdatedDescription(e.target.value)}
            />
          </Form.Group>

          <Form.Group>
            <Form.Label>Price</Form.Label>
            <Form.Control
              type="text"
              value={updatedPrice}
              onChange={(e) => setUpdatedPrice(e.target.value)}
            />
          </Form.Group>
          <Form.Group>
            <Form.Label>Capacity</Form.Label>
            <Form.Control
              type="text"
              value={updatedStock}
              onChange={(e) => setUpdatedStock(e.target.value)}
            />
          </Form.Group>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleCloseModal1}>
            Close
          </Button>
          <Button variant="primary" onClick={updateProduct}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>

      <Modal show={showModal2} onHide={handleCloseModal2}>
        <Modal.Header closeButton>
          <Modal.Title>Add Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form.Group>
            <Form.Label>Name</Form.Label>
            <Form.Control
              type="text"
              value={addedName}
              onChange={(e) => setAddedName(e.target.value)}
            />
          </Form.Group>

          <Form.Group>
            <Form.Label>Description</Form.Label>
            <Form.Control
              type="text"
              value={addedDescription}
              onChange={(e) => setAddedDescription(e.target.value)}
            />
          </Form.Group>

          <Form.Group>
            <Form.Label>Price</Form.Label>
            <Form.Control
              type="text"
              value={addedPrice}
              onChange={(e) => setAddedPrice(e.target.value)}
            />
          </Form.Group>
          <Form.Group>
            <Form.Label>Capacity</Form.Label>
            <Form.Control
              type="text"
              value={addedStock}
              onChange={(e) => setAddedStock(e.target.value)}
            />
          </Form.Group>
          <Form.Group>
            <Form.Label>Image URL</Form.Label>
            <Form.Control
              type="text"
              value={addedImage}
              onChange={(e) => setAddedImage(e.target.value)}
            />
          </Form.Group>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleCloseModal2}>
            Close
          </Button>
          <Button variant="primary" onClick={addProduct}>
            Add Product
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default ProductTable;





































  // import React from 'react';
  // import { Table, Button } from 'react-bootstrap';
  // import { useState, useEffect, useContext} from "react"
  // import UserContext from "../UserContext"
  // import { useParams, Link } from "react-router-dom"


  // const ProductTable = ({ dashProp }) => {
  //   const { _id, /*imageSrc,*/description, name, price, numberOfStock } = dashProp;
  //   const [status, setStatus] = useState('');
  //   const { user } = useContext(UserContext);
  //  console.log(user.token)

  //   useEffect(() => {
  //     // Empty dependency array to run only once on initial render
  //     if (status === '') {
  //       setStatus(dashProp.status);
  //     }
  //   }, [dashProp.status]);

  //   function updateStatus(token) {
  //     const newStatus = status === 'Active' ? 'notActive' : 'Active';
  //     console.log(`http://localhost:4002/product/${_id}`)

  //     fetch(`http://localhost:4002/product/${_id}`, {
  //       method: 'PUT',
  //       headers: {
  //          "Content-type" : "application/json"
          
  //       },
  //       body: JSON.stringify({
  //         status: newStatus,
  //       }),
  //     })
  //       .then((res) => res.json())
  //       .then((data) => {
  //         console.log(data);
  //         setStatus(newStatus); // Update the status in the component state
  //       });
  //   }

  //   return (
  //     <Table striped>
  //       <thead>
  //         <tr>
  //           <th>_id</th>
  //           <th>name</th>
  //           <th>description</th>
  //           <th>price</th>
  //           <th>numberOfStock</th>
  //           <th>Status</th>
  //         </tr>
  //       </thead>
  //       <tbody>
  //         <tr>
  //           <td>{_id}</td>
  //           <td>{name}</td>
  //           <td>{description}</td>
  //           <td>{price}</td>
  //           <td>{numberOfStock}</td>
  //           <td>
  //             <Button type="button" onClick={updateStatus} className="m-2">
  //               {status}
  //             </Button>

  //             <Button type="button" as={Link} to="./modalProductEdit" className="bg-danger">
  //               Update
  //             </Button>
  //           </td> 
  //         </tr>
  //       </tbody>
  //     </Table>
  //   );
  // };

  // export default ProductTable;















  // const ProductTable = ({ dashProp }) => {

  //   const { _id, /*imageSrc,*/ name, price, numberOfStock, status  } = dashProp

  //   const { product } = useContext(UserContext);
  //   const [button, setButton] = useState({status});

  //  useEffect(() => {
  //     if (button === 'active') {
  //       setButton('notActive');
  //     } else {
  //       setButton('active');
  //     }
  //   }, [button]); // Add 'button' as a dependency to avoid infinite loop

   

  //   const toggleButton = () => {
  //     setButton((prevButton) => (prevButton === 'active' ? 'notActive' : 'active'));
  //   };

  //   function statusChange(e){

  //     e.preventDefault();

  //       fetch(`http://localhost:4002/product/${_id}`, {
  //         method: "PUT",
  //         headers: {
  //           "Content-type" : "application/json"
  //         },
  //         body: JSON.stringify({
  //           status: status
  //         })
  //       })
  //       .then(res => res.json())
  //       .then(data => {
  //         console.log(data)
  //       })
  //   }

  //   return (
  //     <Table striped>
  //       <thead>
  //         <tr>
  //           <th>_id</th>
  //           <th>name</th>
  //           <th>price</th>
  //           <th>numberOfStock</th>
  //           <th>Status</th>
  //         </tr>
  //       </thead>
  //       <tbody>
  //           <tr>
  //             <td>{_id}</td>
  //             <td>{name}</td>
  //             <td>{price}</td>
  //             <td>{numberOfStock}</td>
  //             <td>
  //               <Button type="button" onClick={e => statusChange(e.target.value)}>{status}</Button>
  //             </td>
  //           </tr>
  //       </tbody>
  //     </Table>
  //   );
  // };

  // export default ProductTable;


  // import React, { useState, useEffect, useContext } from 'react';
  // import { Table, Button } from 'react-bootstrap';
  // import UserContext from "../UserContext";

  // const ProductTable = ({ dashProp }) => {
  //   const { _id, name, price, numberOfStock, status } = dashProp;
  //   const { product } = useContext(UserContext);
  //   const [button, setButton] = useState(status);

  //   // useEffect(() => {
  //   //   if(button === "active"){
  //   //     setButton("notActive")
  //   //   } else {
  //   //     setButton("active")
  //   //   }
  //   // }, [button]);

  //   const updateStatus = () => {
  //     // Update status in the database
  //     fetch(`http://localhost:4002/product/${_id}`, {
  //       method: "PUT",
  //       headers: {
  //         "Content-type": "application/json"
  //       },
  //       body: JSON.stringify({
  //         status: button
  //       })
  //     })
  //       .then(res => res.json())
  //       .then(data => {
  //         console.log(data);
  //       })
  //       .catch(error => {
  //         console.error("Error:", error);
  //       });
  //   };

  //   const handleStatusChange = () => {
  //     const newStatus = button === 'active' ? 'notActive' : 'active';
  //     setButton(newStatus);

  //     // Update status in the local state immediately
  //     dashProp.status = newStatus;
  //   };

  //   return (
  //     <Table striped>
  //       <thead>
  //         <tr>
  //           <th>_id</th>
  //           <th>name</th>
  //           <th>price</th>
  //           <th>numberOfStock</th>
  //           <th>Status</th>
  //         </tr>
  //       </thead>
  //       <tbody>
  //         <tr>
  //           <td>{_id}</td>
  //           <td>{name}</td>
  //           <td>{price}</td>
  //           <td>{numberOfStock}</td>
  //           <td>
  //             <Button type="button" onClick={updateStatus}>
  //               {button}
  //             </Button>
  //           </td>
  //         </tr>
  //       </tbody>
  //     </Table>
  //   );
  // };

  // export default ProductTable;


  /*import React, { useState, useEffect, useContext } from 'react';
  import { Table, Button } from 'react-bootstrap';
  import UserContext from "../UserContext";

  const ProductTable = ({ dashProp }) => {
    const { _id, name, price, numberOfStock, status } = dashProp;
    const { product } = useContext(UserContext);
    const [buttonStatus, setButtonStatus] = useState(status);

    const updateStatus = (newStatus) => {
      fetch(`http://localhost:4002/product/${_id}`, {
        method: "PUT",
        headers: {
          "Content-type": "application/json"
        },
        body: JSON.stringify({
          status: newStatus
        })
      })
        .then(res => res.json())
        .then(data => {
          console.log(data);
          setButtonStatus(data.status); // Update the button status with the updated value from the backend
        })
        .catch(error => {
          console.error("Error:", error);
        });
    };

    const handleStatusChange = () => {
      const newStatus = buttonStatus === 'active' ? 'notActive' : 'active';
      updateStatus(newStatus); // Call the updateStatus function to update the status in the backend
    };

    return (
      <Table striped>
        <thead>
          <tr>
            <th>_id</th>
            <th>name</th>
            <th>price</th>
            <th>numberOfStock</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>{_id}</td>
            <td>{name}</td>
            <td>{price}</td>
            <td>{numberOfStock}</td>
            <td>
              <Button type="button" onClick={handleStatusChange}>
                {buttonStatus}
              </Button>
            </td>
          </tr>
        </tbody>
      </Table>
    );
  };

  export default ProductTable;*/








  /*import React, { useState, useEffect, useContext } from 'react';
  import { Table, Button } from 'react-bootstrap';
  import UserContext from "../UserContext";

  const ProductTable = ({ dashProp }) => {
    const { _id, name, price, numberOfStock, status } = dashProp;
    const { product } = useContext(UserContext);
    const [button, setButton] = useState(status);

    useEffect(() => {
      updateStatus();
    }, [button]);

    const updateStatus = () => {
      fetch(`http://localhost:4002/product/${_id}`, {
        method: "PUT",
        headers: {
          "Content-type": "application/json"
        },
        body: JSON.stringify({
          status: button
        })
      })
        .then(res => res.json())
        .then(data => {
          console.log(data);
        })
        .catch(error => {
          console.error("Error:", error);
        });
    };

    const handleStatusChange = () => {
      setButton(prevStatus => prevStatus === 'active' ? 'notActive' : 'active');
    };

    return (
      <Table striped>
        <thead>
          <tr>
            <th>_id</th>
            <th>name</th>
            <th>price</th>
            <th>numberOfStock</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>{_id}</td>
            <td>{name}</td>
            <td>{price}</td>
            <td>{numberOfStock}</td>
            <td>
              <Button type="button" onClick={handleStatusChange}>
                {button}
              </Button>
            </td>
          </tr>
        </tbody>
      </Table>
    );
  };

  export default ProductTable;*/
