import ProductCard from "../components/ProductCard"
import { useState, useEffect, useContext } from "react"

export default function Products(){

	const [AllProducts, setAllProducts] = useState([]);
	// Retrieves the courses from database upon initial render of the "Courses" Component

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/product/active`)
		.then(res => res.json())
		.then(data => {
			console.log(data);


			setAllProducts(data.map(product => {
				console.log(product)
				return (
					<ProductCard key={product._id} productProp={product}/>
					)
			}))

		})
	}, [])


	return(

			<>
				{/*<h1>Products</h1>*/}
				{/*Prop making ang prop passing*/}
				{AllProducts}

				<footer className="footer bg-secondary">
				  <div className="container bg-secondary ">
				    <div className="footer-content">
				      <div className="footer-column text-center">
				        <h3>About Us</h3>
				        <p >Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero.</p>
				      </div>
				      <div className="footer-column text-center">
				        <h3>Links</h3>
				        <ul>
				          <li><a href="#">Home</a></li>
				          <li><a href="#rooms">Rooms</a></li>
				          <li><a href="#">Contact</a></li>
				        </ul>
				      </div>
				      <div className="footer-column">
				        <h3>Subscribe</h3>
				        <p>Subscribe to our newsletter to get updates and offers.</p>
				        <form className="subscribe-form" action="#">
				          <input type="email" name="email" placeholder="Enter your email" />
				          <button type="submit">Subscribe</button>
				        </form>
				      </div>
				    </div>
				  </div>
				 
				</footer>
			</>


		)
}