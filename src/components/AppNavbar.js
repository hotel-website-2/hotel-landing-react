import { Nav, Navbar } from "react-bootstrap";
import { Link } from "react-router-dom";
import Login from "../pages/Login";
import UserContext from "../UserContext";
import { useContext } from "react";

export default function AppNavbar() {
  const { user } = useContext(UserContext);

  return (
    <>
      {user.isAdmin ? (
        <Navbar bg="light" expand="lg" className="px-3 Navbar">
          <Navbar.Brand as={Link} to={"/"} className="fw-bold logo">
            AWH
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="navlinks">
              <Nav.Link as={Link} to={"/"}>
                Home
              </Nav.Link>
              <Nav.Link as={Link} to="/product">
                Rooms
              </Nav.Link>
              <Nav.Link as={Link} to={"/dashboard"}>
                Dashboard
              </Nav.Link>
              {user.token !== null ? (
                <Nav.Link as={Link} to={"/logout"}>
                  Logout
                </Nav.Link>
              ) : (
                <>
                  <Nav.Link as={Link} to={"/register"}>
                    Register
                  </Nav.Link>
                  <Nav.Link as={Link} to={"/login"}>
                    Login
                  </Nav.Link>
                </>
              )}
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      ) : (
        <Navbar bg="light" expand="lg" className="px-3 Navbar">
          <Navbar.Brand as={Link} to={"/"} className="fw-bold logo">
            AWH
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav nav-collapse">
            <Nav className="navlinks">
              <Nav.Link as={Link} to={"/"}>
                Home
              </Nav.Link>
              <Nav.Link as={Link} to="/product">
                Rooms
              </Nav.Link>
              <Nav.Link as={Link} to="/activities">
                Activities
              </Nav.Link>
              <Nav.Link as={Link} to={"/order"}>
                Booking
              </Nav.Link>
              {user.token !== null ? (
                <Nav.Link as={Link} to={"/logout"}>
                  Logout
                </Nav.Link>
              ) : (
                <>
                  <Nav.Link as={Link} to={"/register"}>
                    Register
                  </Nav.Link>
                  <Nav.Link as={Link} to={"/login"}>
                    Login
                  </Nav.Link>
                </>
              )}
            </Nav>
          </Navbar.Collapse>
          <div
            as={Link}
            to={"/product"}
            className="book-btn d-flex justify-content-center align-items-center"
          >
            <a href="/product" className="link-nav">
              Book Now!
            </a>
          </div>
        </Navbar>
      )}
    </>
  );
}
