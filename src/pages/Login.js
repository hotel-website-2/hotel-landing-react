import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from "../UserContext"
import { Navigate } from "react-router-dom";
import Swal from "sweetalert2"
import { Stanza } from 'react-hypertext';
import { Link } from "react-router-dom"

export default function Login() {
    const { user, setUser } = useContext(UserContext);

    // State hooks to store the values of the input fields
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(true);

    useEffect(() => {

        // Validation to enable submit button when all fields are populated and both passwords match
        if(email !== '' && password !== ''){
            setIsActive(true);
        }else{
            setIsActive(false);
        }

    }, [email, password]);

    function authenticate(e) {

        // Prevents page redirection via form submission
        e.preventDefault();

        // Process a fetch request to the corresponding API

        fetch(`${process.env.REACT_APP_API_URL}/user/login`, {
            method: "POST",
            headers: {
                "Content-type" : "application/json"
            },
            body: JSON.stringify({
                email : email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            // If no user info is found, the "access" property will not be available
            if(typeof data.access !== "undefined"){
                localStorage.setItem("token", data.access);
                retrieveUserDetails(data.access);

                Swal.fire({
                  icon: 'success',
                  title: 'Login Successful!',
                  text: 'Welcome to Zuitt!',
                })

            }else{
                Swal.fire({
                  icon: 'error',
                  title: 'Authentication Failed!',
                  text: 'Please try again!',
                })
            }
        })

        // setUser({
        //     // Store the email in the localStroge
        //     email: localStorage.setItem("email", email)
        // })

        // Clear input fields after submission
        setEmail('');
        setPassword('');


    }

     // Retrieve user details using its token
    const retrieveUserDetails = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/user/details`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            setUser({
                id: data._id,
                isAdmin: data.isAdmin,
                email: data.email
            })
        })
    }





    return (


        (user.token !== null) ?
            <Navigate to="/product"/>
        :
        
        
        <div className="container-fluid pt-5 mt-5 ">
            
            <div className="row d-flex justify-content-center">
                    <div className="col-md-5 login-img">
                        <img src="https://media.istockphoto.com/id/104731717/photo/luxury-resort.jpg?s=612x612&w=0&k=20&c=cODMSPbYyrn1FHake1xYz9M8r15iOfGz9Aosy9Db7mI="/>
                    </div>

                    <div className="col-md-5 form">
                        <h1 className="login-txt text-center pt-3">Login Your Account</h1>
                        <Form onSubmit={(e) => authenticate(e)} className="my-5">
                        <div controlId="userEmail"  id="userEmail" className=" d-flex justify-content-center pt-3">
                       
                            <input
                                type="email" 
                                placeholder="Enter email" 
                                required
                                value={email}
                                onChange={(e) => setEmail(e.target.value)}
                                required
                                className="w-1/2"
                                controlId="userEmail" 
                            />
                        </div>

                        <div controlId="password" className="d-flex justify-content-center pt-5">
                          
                            <input 
                                type="password" 
                                placeholder="Password" 
                                required
                                value={password}
                                onChange={(e) => setPassword(e.target.value)}
                                required
                                className=""
                                controlId="password" 
                            />
                        </div>

                        { isActive ? 
                            <Button variant="success" type="submit" id="submitBtn" className="mt-3">
                                Login
                            </Button>
                            : 
                            <Button variant="danger" type="submit" id="submitBtn" className="mt-3" disabled>
                                Login
                            </Button>
                        }

                    </Form>

                        <p>No account yet? <Link to="/register">Click here</Link></p>
                    </div>
                </div>

            </div>
        
    )
}
