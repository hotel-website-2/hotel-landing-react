import Banner from "../components/Banner"
import { Link } from "react-router-dom"
import { Container, Row, Col, Form, Button } from "react-bootstrap";


export default function Home(){

	const data = {
		title: "I am Groot",
		content: "Goot is dumb as fck",
		destination: "./courses",
		label: "Subscribe"
	}

	
	return (
		<>

			<div className="landing-img container-fluid">
				<div className="row">
					<div className="col-sm-7 pt-5 txt-landing-container text-light">

					<h1 className="welcome pt-5"><span className="W">W</span>elcome</h1>	
					<h1 className="awesome"><span className="A">A</span>wesome</h1>	
					<div as={Link} to={"/product"} className="book-btn-home d-flex justify-content-center align-items-center"><a href="/product" className="link">Book Now!</a>
					</div>

				</div>
				</div>
			</div>

			<div className="container-fluid pt-5" id="rooms">

				{/* ROOMS SECTION */}
					<h1	 className="rooms-headline text-center pb-0">MODERN STYLISH ROOMS</h1>
					<div className="small-gold-line-1 pb-3"></div>
					<div className="rooms-des-container">

					<p className="rooms-headline-description pb-2 text-center">
					A total of 113 cozy rooms with a spacious balcony, a view of infinity pool, and sea view that can absolutely make for a memorable, enjoyable and awesome stay. Each room exudes not only with the hotel’s gleaming aesthetic but also the modern comforts of a luxury hotel & resort.

					Awesome Hotel can ensure esteemed guests that they will undoubtedly feel relaxed and pampered throughout their stay. These amenities also help to create a luxurious atmosphere, which can make guests feel like they are truly on a special and memorable vacation.</p>
				</div>


				{/*ROOMS CARD*/}
				<div className="row d-flex justify-content-center slider">

				{/* Image 1*/}
					<div className="col-md-2">
						<img className="image1" width="250" src="https://image-tc.galaxy.tf/wijpeg-78hgbyxrzaglyq6s8h14taih3/the-may-fair-bedroom-junior-suite-1-1_portrait.jpg?crop=480%2C0%2C960%2C1280&width=800" />
					</div>

				{/* Image 2*/}
					<div className="col-md-2">
						<img className="image2" width="250" src="https://static.arocdn.com/Sites/50/downhall/uploads/images/PanelImages/General/Mansion_Suite_portrait.jpg" />
					</div>

				{/* Image 3*/}
					<div className="col-md-2">
						<img className="image3" width="250" src="https://www.dorchestercollection.com/wp-content/uploads/BHH-Supeior-room-portrait-825x1100.jpg" />
					</div>

				{/* Image 4*/}
					<div className="col-md-2">
						<img className="image4" width="250" src="https://www.dorchestercollection.com/wp-content/uploads/beverly-hills-garden-suite-18A-bedroom_Portrait-872x1164.jpg" />
					</div>

				{/* Image 5*/}
					<div className="col-md-2">
						<img className="image5" width="250" src="https://media.istockphoto.com/id/1153225644/photo/3d-render-of-luxury-hotel-room.jpg?s=612x612&w=0&k=20&c=RlWB-SdsgfF3k4cIRv-kqcc1mNuAmxyNjST7ajvK7PI=" />
					</div>

				</div>


			</div>


			<div className="d-flex justify-content-center pt-3">

				{/* See all Button */}
				<div className="see-all-rooms-btn d-flex justify-content-center align-items-center">
						<a href="/product" className="link-rooms">SEE ALL ROOMS</a>
				</div>

			</div>



			<div className="container-fluid dining-page-container pt-5 mt-5 pb-5">
				<div className="row">


					<div className="col-md-5 dining-img-container">
						<img className="img-fluid dining-img" src="https://www.panoramictrip.com/wp-content/uploads/2019/10/DSC02585_0.jpg" />
					</div>

					<div className="col-md-4 dining-des-container pt-4 text-center">
						<h1 className="dining-headline ">HOTEL DINING</h1>
						<div className="small-gold-line-2 pb-3"></div>

						<p className="dining-des">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel sapien eget quam lobortis congue quis non nisi. Maecenas nec semper magna.
						</p>

						<p className="dining-des">Nam non purus placerat, lacinia lorem vel, vehicula massa. Nunc hendrerit pretium nisi quis gravida. Praesent ornare dictum est. Integer porta commodo ligula vel tempor. Donec at sapien ut mauris hendrerit blandit.
						</p>
					
					</div>

				</div>
			</div>

				{/* MENU CONTAINER */}

			<div className="container d-flex justify-content-center py-2 menu-container my-5">
				<div className="row-fluid d-flex justify-content-center menu">

						{/* Morning menu*/}
						<div className="col-4">
							<img className="img-fluid" src="https://scontent.fmnl17-4.fna.fbcdn.net/v/t1.6435-9/191692269_332597351596146_1027856689533086943_n.jpg?_nc_cat=104&ccb=1-7&_nc_sid=c4c01c&_nc_eui2=AeEA0S_J9NK5e5lsRNKYalyZsZyTz396wMixnJPPf3rAyEQMQFvkkyaRFdbeotH_K7mCTQwmAvDq4AkngDuzJg7v&_nc_ohc=HAX6z12MwVAAX_pBzQS&_nc_ht=scontent.fmnl17-4.fna&oh=00_AfCbR2abst0FaOjZ7FiAwCh9SfFXw3Tz4JkxqRhqJsqnGg&oe=64A0BB2C" />
						</div>

						{/* menu description*/}
						<div className="col-4 menu-des d-flex align-items-center px-3 text-justify">

							<img className="img-fluid" src="https://www.sophisticatedgourmet.com/wp-content/uploads/2016/11/food-quote-hippocrates.png"/>
							{/*<h4>“Part of the secret of success is to eat what you like and let the food fight it out inside.”
							</h4>*/}
						</div>

						{/* Appetizer menu*/}
						<div className="col-4 appetizer-menu-img">
							<img className="img-fluid" src="https://scontent.fmnl17-1.fna.fbcdn.net/v/t1.6435-9/187461649_332597141596167_5279657652412254379_n.jpg?_nc_cat=100&ccb=1-7&_nc_sid=c4c01c&_nc_eui2=AeHjKlla4yhdM0s3TERYS640ZlDZ3V5h8V5mUNndXmHxXiPnhS5Qqpw4qkxpjqUEh-2I6fysemnTiLJS1Cr9tZGa&_nc_ohc=Z_v5SljtIfoAX-aGvNi&_nc_ht=scontent.fmnl17-1.fna&oh=00_AfAyT2nmz3bQprZcsZLfgadaxqRaENDXlktG-DmaHZjPXw&oe=64A0BF27"/>
						</div>

				</div>
						

			</div>


			<section id="contact-us" className="py-5">
			  <div className="contact-bg">
			    <Container className="container-fluid ">
			      <Row>
			        <Col md={6}>
			          <h2 className="text-center mb-5">Contact Us</h2>
			          <Form>
			            <Form.Group controlId="formName">
			              <Form.Control type="text" placeholder="Name" />
			            </Form.Group>

			            <Form.Group controlId="formEmail">
			              <Form.Control type="email" placeholder="Email" />
			            </Form.Group>

			            <Form.Group controlId="formMessage">
			              <Form.Control
			                as="textarea"
			                rows={5}
			                placeholder="Message"
			              />
			            </Form.Group>

			            <Button variant="primary" type="submit">
			              Submit
			            </Button>
			          </Form>
			        </Col>
		
			      </Row>
			    </Container>
			  </div>
			</section>

			<footer className="footer bg-secondary">
			  <div className="container bg-secondary ">
			    <div className="footer-content">
			      <div className="footer-column text-center">
			        <h3>About Us</h3>
			        <p >Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero.</p>
			      </div>
			      <div className="footer-column text-center">
			        <h3>Links</h3>
			        <ul>
			          <li><a href="#">Home</a></li>
			          <li><a href="#rooms">Rooms</a></li>
			          <li><a href="#">Contact</a></li>
			        </ul>
			      </div>
			      <div className="footer-column">
			        <h3>Subscribe</h3>
			        <p>Subscribe to our newsletter to get updates and offers.</p>
			        <form className="subscribe-form" action="#">
			          <input type="email" name="email" placeholder="Enter your email" />
			          <button type="submit">Subscribe</button>
			        </form>
			      </div>
			    </div>
			  </div>
			 
			</footer>


			
		</>



		)
}


