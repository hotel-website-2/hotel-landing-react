import React, { useState, useContext } from 'react';
import { Button, Card, Modal, Form } from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function ProductCard({ productProp }) {
  const { user } = useContext(UserContext);
  const { _id, name, adults, children, capacity, description, price, numberOfStock, image, checkIn, checkOut } = productProp;
  const [showModal, setShowModal] = useState(false);
  const [images, setImages] = useState([image]);

  const [bookingInfo, setBookingInfo] = useState({
    checkIn: '',
    checkOut: '',
    adults: '',
    children: '',
  });


  const handleCloseModal = () => {
    setShowModal(false);
  };

  const handleShowModal = () => {
    setShowModal(true);
  };

  const buyNow = () => {
    fetch(`${process.env.REACT_APP_API_URL}/order/orders`, {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${user.token}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        products: [
          {
            productId: _id,
            checkIn: bookingInfo.checkIn,
            checkOut: bookingInfo.checkOut,
          },
        ],
        name: name,
        image: image,
        adults: bookingInfo.adults,
        children: bookingInfo.children,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        Swal.fire({
          icon: 'success',
          title: 'Booking Received!',
          text: 'Thank you for booking with us!',
        });
      });
  };


  const handleImageChange = (event) => {
    const files = Array.from(event.target.files);
    const imageUrls = [];

    files.forEach((file) => {
      const reader = new FileReader();
      reader.onload = () => {
        imageUrls.push(reader.result);
      };
      reader.readAsDataURL(file);
    });

    setImages(imageUrls);
  };

  return (

    /* Rooms Section*/
    <div className="container py-1">

      <div className="row pb-3">
          <div className="col-8">

              <div className="container rooms-page-container">

                  <div className="row">

                      <div className="col-4 py-2 px-4">
                          {images.map((imageUrl, index) => (
                                 <img src={imageUrl} alt={`Product Image ${index + 1}`} key={index} className="images" height="200vh" width="270vw"/>
                                 ))}
                      </div>

                      <div className="col-6 des-container pt-4">
                          <h2 className="room-name">{name}</h2>
                          <p>{description}</p>
                          <p>Maximum capacity of {capacity}</p>
                          <p className="price text-secondary">Price: {price}</p>
                          {!user.isAdmin && (
                            <Button className="rooms-booknow-bt" onClick={handleShowModal}>Book Now</Button>
                          )}
                      </div>

                  </div>

              </div>
          </div>

          {/* Comments Section*/}
          <div className="col-3 comments">

              <div className="">
                  <img className="profile-pic-comments" src="https://www.shutterstock.com/image-vector/male-profile-image-circle-color-260nw-1520981711.jpg"/>
                  <p className="commenter-name pr-5">Aifel Jake</p>
                  <p className="date">May 17, 2023</p>
              </div>

              <div className="stars">
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star"></span>
              </div>

              <div>
                  <p className="px-2">It is a wonderful place. It is cozy and made me feel at home. the staffs are also nice and commendable.</p>
              </div>

          </div>

      </div>


      <Modal show={showModal} onHide={handleCloseModal}>
        <Modal.Header closeButton>
          <Modal.Title>Book Now</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form.Group>
            <Form.Label>Name</Form.Label>
            <Form.Control type="text" value={name} />
          </Form.Group>

          <Form.Group>
            <Form.Label>Description</Form.Label>
            <Form.Control type="text" value={description} />
          </Form.Group>

          <Form.Group>
            <Form.Label>Price</Form.Label>
            <Form.Control type="text" value={price} />
          </Form.Group>

          <Form.Group>
            <Form.Label>Check-in</Form.Label>
            <Form.Control
              type="date"
              value={bookingInfo.checkIn}
              onChange={(e) =>
                setBookingInfo({ ...bookingInfo, checkIn: e.target.value })
              }
            />
          </Form.Group>

          <Form.Group>
            <Form.Label>Check-out</Form.Label>
            <Form.Control
              type="date"
              value={bookingInfo.checkOut}
              onChange={(e) =>
                setBookingInfo({ ...bookingInfo, checkOut: e.target.value })
              }
            />
          </Form.Group>

          <Form.Group>
            <Form.Label>Adults</Form.Label>
            <Form.Control
              type="number"
              value={bookingInfo.adults}
              onChange={(e) =>
                setBookingInfo({ ...bookingInfo, adults: e.target.value })
              }
            />
          </Form.Group>

          <Form.Group>
            <Form.Label>Children</Form.Label>
            <Form.Control
              type="number"
              value={bookingInfo.children}
              onChange={(e) =>
                setBookingInfo({ ...bookingInfo, children: e.target.value })
              }
            />
          </Form.Group>

        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleCloseModal}>
            Close
          </Button>
          <Button variant="secondary" onClick={buyNow}>
            Book Now
          </Button>
          <Button variant="primary">Add to Cart</Button>
        </Modal.Footer>
      </Modal>
      </div>
  );
}















// import Button from 'react-bootstrap/Button';
// import Card from 'react-bootstrap/Card';
// import { Link } from "react-router-dom"
// import { Modal, Form } from 'react-bootstrap';
// import { useState } from "react"
 
// export default function productCard({productProp}){

// 	const { _id, /*imageSrc,*/ name, description, price, numberOfStock  } = productProp;
// 	const [showModal, setShowModal] = useState(false);

// 	const orderProduct = () => {
// 		fetch(`http://localhost:4002/product/${_id}`, {
// 			method: "GET",
// 			header: {
// 				"Content-type" : "application/json"
// 			},
// 		})
// 		.then(res => res.json())
// 		.then(data => {
// 			console.log(data)
// 			console.log(data.name)
// 			handleCloseModal()
// 		})
// 	}

// 	const handleCloseModal = () => {
// 	  setShowModal(false);
// 	};

// 	const handleShowModal = () => {
// 	  setShowModal(true);
// 	};


// 	return (
// 		<>
// 			<Card style={{ width: '18rem' }}>
// {/*			     <Card.Img variant="top" src={imageSrc} />*/}
// 			     <Card.Body>
// 			       <Card.Title>{name}</Card.Title>
// 			       <Card.Text>
// 			         {description}
// 			       </Card.Text>
// 			       <Card.Text>
// 			         Price: {price}
// 			       </Card.Text>
// 			       <Card.Text>
// 			         {numberOfStock}
// 			       </Card.Text>
// 			     </Card.Body>
// 			     <Button onClick={handleShowModal}>Place Order</Button>
// 			   </Card>



// 			<Modal show={showModal} onHide={handleCloseModal}>
// 			  <Modal.Header closeButton>
// 			    <Modal.Title>Update Product</Modal.Title>
// 			  </Modal.Header>
// 			  <Modal.Body>
// 			    <Form.Group>
// 			      <Form.Label>Name</Form.Label>
// 			      <Form.Control
// 			        type="text"
// 			        value={name}
			        
// 			      />
// 			    </Form.Group>

// 			    <Form.Group>
// 			      <Form.Label>Description</Form.Label>
// 			      <Form.Control
// 			        type="text"
// 			        value={description}
			  
// 			      />
// 			    </Form.Group>

// 			    <Form.Group>
// 			      <Form.Label>Price</Form.Label>
// 			      <Form.Control
// 			        type="text"
// 			        value={price}
// 			      />
// 			    </Form.Group>
// 			    <Form.Group>
// 			      <Form.Label>Number of Stock</Form.Label>
// 			      <Form.Control
// 			        type="text"
// 			        value={numberOfStock}
// 			      />
// 			    </Form.Group>
// 			  </Modal.Body>
// 			  <Modal.Footer>
// 			    <Button variant="secondary" onClick={handleCloseModal}>
// 			      Close
// 			    </Button>
// 			    <Button variant="primary">
// 			      Save Changes
// 			    </Button>
// 			  </Modal.Footer>
// 			</Modal>

// 		</>

// 		)


// }

