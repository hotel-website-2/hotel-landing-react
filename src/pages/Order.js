// import OrderCard from "../components/OrderCard";
import React, { useState, useEffect, useContext } from "react";
import { useParams } from "react-router-dom";
import UserContext from "../UserContext";

export default function Order() {
  const [orders, setOrders] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/order/`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setOrders(data);
      })
      .catch((error) => {
        console.error("Error fetching orders:", error);
      });
  }, []);

  return (
    <>
      {orders.map((order) => (
        <div key={order._id} className="container-fluid bg-img-booking pt-5 mt-3 pb-5 mb-5">
          <div className="row d-flex justify-content-center align-items-center">

              <div className="col-5 booking-container pt-3">

                  <img className="order-image" height="350vh" src={order.productImage}/>

                  <div className="booking-details">
                     <h2 className="booking-headline-2">Booking Details</h2>
                    <p>Name: {order.productName}</p>
                    <p>Total Amount: {order.totalAmount}</p>
                    <p>Adults: {order.adults}</p>
                    <p>Children : {order.children}</p>
                    <p>Booked Date: {order.purchasedOn}</p>
                    <p>Check-in Date: {order.products[0].checkIn}</p>
                    <p>Check-out Date: {order.products[0].checkOut}</p>

                  </div>

              </div>


          </div>
        </div>
      ))}

      <footer className="footer bg-secondary">
        <div className="container bg-secondary ">
          <div className="footer-content">
            <div className="footer-column text-center">
              <h3>About Us</h3>
              <p >Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero.</p>
            </div>
            <div className="footer-column text-center">
              <h3>Links</h3>
              <ul>
                <li><a href="#">Home</a></li>
                <li><a href="#rooms">Rooms</a></li>
                <li><a href="#">Contact</a></li>
              </ul>
            </div>
            <div className="footer-column">
              <h3>Subscribe</h3>
              <p>Subscribe to our newsletter to get updates and offers.</p>
              <form className="subscribe-form" action="#">
                <input type="email" name="email" placeholder="Enter your email" />
                <button type="submit">Subscribe</button>
              </form>
            </div>
          </div>
        </div>
       
      </footer>
      
    </>
  );
}




/*import React, { useEffect, useState } from 'react';

const OrderList = ({ userId }) => {
  const [orders, setOrders] = useState([]);

  useEffect(() => {
    fetchOrders();
  }, [userId]);

  const fetchOrders = async () => {
    try {
      const response = await fetch(`/api/orders/${userId}`);
      const data = await response.json();
      setOrders(data);
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <div>
      <h1>Orders</h1>
      {orders.length === 0 ? (
        <p>No orders found for user {userId}.</p>
      ) : (
        <ul>
          {orders.map((order) => (
            <li key={order._id}>
              <strong>Order ID:</strong> {order._id}
              <br />
              <strong>Product Name:</strong> {order.productName}
              <br />
              <strong>Quantity:</strong> {order.quantity}
            </li>
          ))}
        </ul>
      )}
    </div>
  );
};

export default OrderList;*/














// import OrderCard from "../components/OrderCard"
// import { useState, useEffect, useContext } from "react"

// export default function Products(){

//   const [AllOrder, setAllOrders] = useState([]);
//   // Retrieves the courses from database upon initial render of the "Courses" Component

//   useEffect(() => {
//     fetch(`http://localhost:4002/order/${user._id}`, {
//       method: "GET",
//       headers: {
//         "Content-type": "application/json"
//       }
//     })
//       .then((res) => res.json())
//       .then((data) => {
//         console.log(data);
//         setAllOrders(data)
//       });
//   }, []);


//   return(

//       <>
//         <h1>Products</h1>
//         {/*Prop making ang prop passing*/}
//         {AllOrder}
//       </>


//     )
// }



/*
import { Link } from "react-router-dom"
import ProductCard from "../components/ProductCard"
import { useState, useEffect, useContext } from "react"
import { Button } from "react-bootstrap"
import UserContext from "../UserContext"

export default function PlaceOrder({orderProp}){

  const { _id, name, description, price, numberOfStock } = orderProp
	const [ orders, setOrders] = useState("kk")
	const { user } = useContext(UserContext)
	
	useEffect(() => {

		fetch(`http://localhost:4002/product/${_id}`, {
			method: "GET",
			header: {
				"Content-type" : "application/json"
			},
			body: JSON.stringify({
				
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
		})
	})
	return (
			<>
			<h1>Orders</h1>
			<Button >Checkout</Button>
			</>
		)
}
*/



/*import React, { useState } from 'react';

const OrderForm = () => {
  // State variables for storing form data
  const [products, setProducts] = useState([]);
  const [errorMessage, setErrorMessage] = useState('');

  // Handle form submission
  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      // Send a POST request to the backend API
      const response = await fetch('/api/orders', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'your-auth-token',
        },
        body: JSON.stringify({ products }),
      });

      if (response.ok) {
        const order = await response.json();
        console.log('Order created:', order);
        // Reset the form and display a success message
        setProducts([]);
        setErrorMessage('');
      } else {
        const error = await response.json();
        throw new Error(error.message);
      }
    } catch (error) {
      console.error('Error creating order:', error);
      setErrorMessage(error.message);
    }
  };

  // Handle changes to the form fields
  const handleProductChange = (e, index) => {
    const { name, value } = e.target;
    const newProducts = [...products];
    newProducts[index] = { ...newProducts[index], [name]: value };
    setProducts(newProducts);
  };

  // Add a new product input field
  const handleAddProduct = () => {
    setProducts([...products, { productId: '', quantity: 1 }]);
  };

  // Remove a product input field
  const handleRemoveProduct = (index) => {
    const newProducts = [...products];
    newProducts.splice(index, 1);
    setProducts(newProducts);
  };

  return (
    <div>
      <h2>Order Form</h2>
      <form onSubmit={handleSubmit}>
        {products.map((product, index) => (
          <div key={index}>
            <label>
              Product ID:
              <input
                type="text"
                name="productId"
                value={product.productId}
                onChange={(e) => handleProductChange(e, index)}
              />
            </label>
            <label>
              Quantity:
              <input
                type="number"
                name="quantity"
                value={product.quantity}
                onChange={(e) => handleProductChange(e, index)}
              />
            </label>
            <button type="button" onClick={() => handleRemoveProduct(index)}>
              Remove
            </button>
          </div>
        ))}
        <button type="button" onClick={handleAddProduct}>
          Add Product
        </button>
        <button type="submit">Submit Order</button>
      </form>
      {errorMessage && <p>{errorMessage}</p>}
    </div>
  );
};

export default OrderForm;*/
